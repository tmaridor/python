#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exercice 3.
Ecrivez le code python permettant de calculer la distribution des notes des
films proposés sur movielens. Vous utiliserez le dataset "100k" :
    
Created on Mon Dec 19 15:57:23 2016
u.data     -- The full u data set, 100000 ratings by 943 users on 1682 items.
              Each user has rated at least 20 movies.  Users and items are
              numbered consecutively from 1.  The data is randomly
              ordered. This is a tab separated list of 
	         user id | item id | rating | timestamp. 
              The time stamps are unix seconds since 1/1/1970 UTC  
              http://www.gregreda.com/2013/10/26/using-pandas-on-the-movielens-dataset/
@author: tibo
"""

#import des data du fichier u.data
from os import chdir,getcwd
import csv
chdir("/home/tibo/Documents/esigelec/python/ml-100k/")#on se place dans le dossier contenant les data
rep_cour = getcwd()
print("répertoire courant:",rep_cour)

import pandas as pd
#on détermine le nom des colonnes
rnames = ['user_id','movie_id','rating','timestamp']
#on récupere les données du fichier u.data que l'on place dans une dataframe
ratings = pd.read_table('u.data',sep='\t',header=None,names=rnames)
#ainsi que les nom des films
m_cols = ['movie_id', 'title']
movies = pd.read_csv('u.item', sep='|', names=m_cols, usecols=range(2),
                     encoding='latin-1')
#puis on assemble rating et movies
movie_ratings = pd.merge(movies, ratings)
print(movie_ratings[0:10])
print('nombre de vote 1 2 3 4 5 attribué:\n',movie_ratings.rating.value_counts())
print('nombre de votants par film:\n',movie_ratings.title.value_counts())
print('nombre de vote 12345 attribué groupé par film ou distribution des notes par film :\n',movie_ratings.groupby('title').rating.value_counts())


